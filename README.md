## Information
### This repository contains analysis and figure-generating scripts in _R_ of the following manuscript:

Mohammedyaseen Syedbasha, Ferdinando Bonfiglio, Janina Linnik, Claudia Stuehler, Daniel Wüthrich & Adrian Egli: Interferon-lambda enhances the differentiation of naive B-cells into plasmablasts via mTORC1 pathway, __2020__.

*Corresponding authors:
Mohammedyaseen Syedbasha <m.syedbasha@unibas.ch>, 
Adrian Egli <adrian.egli@usb.ch>

## Requirements
* _R_ version >= 3.6
* _R_ packages: ggsignif (0.6),  ggpubr (0.2),  magrittr (1.5), cowplot (1.0), ggplot2 (3.2), here (0.1), readr (1.3), tidyr (1.0), dplyr (0.8), scales (1.0)

## Author
* Mohammedyaseen Syedbasha <m.syedbasha@unibas.ch> (Data)
* Janina Linnik <janina.linnik@bsse.ethz.ch> (Scripts)

## Licence
Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
