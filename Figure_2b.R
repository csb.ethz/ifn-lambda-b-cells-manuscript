#!/usr/bin/env Rscript
# This script produces Figure 2b of the following manuscript:
# Title:   Interferon-lambda enhances the differentiation of naive B-cells into
#          plasmablasts via mTORC1 pathway
# Authors: Mohammedyaseen Syedbasha*, Ferdinando Bonfiglio, Janina Linnik,
#          Claudia Stuehler, Daniel Wüthrich & Adrian Egli*
# *Corresponding authors:
#          m.syedbasha@unibas.ch; adrian.egli@usb.ch
#
# In:     data.csv
# Out:    Figure_2b.pdf
# Date:   October 14th, 2019
# Author: Janina Linnik <jlinnik@ethz.ch>
#
# (C) Copyright 2019, 2020 ETH Zurich, D-BSSE, Janina Linnik
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

library(dplyr)
library(tidyr)
library(readr)
library(here)
library(cowplot)
library(ggplot2)

# Load plot theme
source(paste0(here::here(), "/theme_plot.R"))

data <- read_csv(paste0(here::here(), "/data.csv")) %>%
  dplyr::filter(figure == "2b") %>%
  dplyr::mutate(value = value / 100) %>%
  dplyr::group_by(treatment, time) %>%
  dplyr::mutate(mean = mean(value), sd = sd(value)) %>%
  dplyr::ungroup()

data <- data %>%
  dplyr::mutate(
    treatment = factor(
      treatment,
      levels = c("control", "lambda", "igm", "igm.lambda")
    )
  )

p <- ggplot(data) +
  geom_smooth(
    aes(time, value, group = treatment, color = treatment), lwd = 0.5
  ) +
  geom_errorbar(
    aes(time, value, ymin = mean - sd, ymax = mean + sd), 
    size = 0.3, width = 1
  ) +
  geom_point(
    aes(time, value, fill = treatment, group = treatment), 
    colour = "black", pch = 21, size = 1.5, lwd = 0.5
  ) +
  theme_plot(font_size = 11) +
  scale_color_manual(
    labels = c(
      "Control",
      expression(`IFN-` * lambda),
      "IgM",
      expression(`IgM + IFN-` * lambda)
    ),
    values = c("#929292", "#EA4025", "#1431F5", "#F09937"),
    name = "Treatments"
  ) +
  scale_fill_manual(
    labels = c(
      "Control",
      expression(`IFN-` * lambda),
      "IgM",
      expression(`IgM + IFN-` * lambda)
    ),
    values = c("#929292", "#EA4025", "#1431F5", "#F09937"),
    name = "Treatments"
  ) +
  scale_x_continuous(
    breaks = unique(data$time),
    name = "Time (hours)"
  ) +
  scale_y_continuous(
    breaks = seq(0, 0.3, 0.1),
    labels = c(0, 10, 20, 30),
    limits = c(0, 0.3),
    name = "pS6+ cells (%)"
  )
p
ggsave(
  paste0("./Figure_2b.pdf"),
  p,
  width = 11, height = 6, units = "cm", dpi = 720
)
